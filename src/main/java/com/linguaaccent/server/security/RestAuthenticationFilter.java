package com.linguaaccent.server.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linguaaccent.server.dto.LoginRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.platform.commons.util.StringUtils.isBlank;

@Slf4j
public class RestAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final ObjectMapper objectMapper;


    public RestAuthenticationFilter(String defaultFilterProcessesUrl, ObjectMapper objectMapper) {
        super(defaultFilterProcessesUrl);
        this.objectMapper = objectMapper;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {

        log.debug("Login authentication form submission processing");

        if (!request.getMethod().equals("POST")) {
            log.error("Authentication method not supported: {}", request.getMethod());
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }

        LoginRequest loginRequest = objectMapper.readValue(request.getReader(), LoginRequest.class);

        log.debug("Login request form retrieved: {}", loginRequest);

        if (isBlank(loginRequest.getEmail()) || isBlank(loginRequest.getPassword())) {
            log.error("Credentials not provided");
            //throw exception

        }

        EmailPasswordAuthenticationToken emailPasswordAuthenticationToken =
                new EmailPasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());

        return this.getAuthenticationManager().authenticate(emailPasswordAuthenticationToken);

    }


}
