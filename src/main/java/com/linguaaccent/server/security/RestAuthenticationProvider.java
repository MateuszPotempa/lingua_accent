package com.linguaaccent.server.security;

import com.linguaaccent.server.model.UserPrincipal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class RestAuthenticationProvider implements AuthenticationProvider {

    private final PasswordEncoder passwordEncoder;
    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public RestAuthenticationProvider(PasswordEncoder passwordEncoder, UserDetailsServiceImpl userDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        log.debug("Attempting user authentication with provided credentials");

        EmailPasswordAuthenticationToken authenticationToken = (EmailPasswordAuthenticationToken) authentication;
        String email = authenticationToken.getEmail();
        String password = authenticationToken.getPassword();

        UserDetails userDetails = userDetailsService.loadUserByUsername(email);

        if (Objects.isNull(userDetails)) {
            log.error("Authentication failed! User with provided email: {} do not exist in database", email);
//            throw exception

        }

        if (!passwordEncoder.matches(password, userDetails.getPassword())) {
            log.error("Authentication failed! Password is not correct");
        }

        UserPrincipal userPrincipal = (UserPrincipal) userDetails;

        EmailPasswordAuthenticationToken userAuthentication =
                new EmailPasswordAuthenticationToken(userPrincipal.getEmail(), null,null);

        SecurityContextHolder.getContext().setAuthentication(userAuthentication);

        return userAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(EmailPasswordAuthenticationToken.class);
    }
}
