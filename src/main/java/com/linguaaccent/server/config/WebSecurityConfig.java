package com.linguaaccent.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linguaaccent.server.security.RestAuthenticationFilter;
import com.linguaaccent.server.security.RestAuthenticationProvider;
import com.linguaaccent.server.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final RestAuthenticationEntryPoint authenticationEntryPoint;
    private final RestAuthenticationProvider authenticationProvider;
    private final ObjectMapper objectMapper;
    private static final String DEFAULT_FILTER_PROCESSES_URL="/api/auth/login";


    @Autowired
    public WebSecurityConfig(RestAuthenticationEntryPoint authenticationEntryPoint, RestAuthenticationProvider authenticationProvider, ObjectMapper objectMapper) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.authenticationProvider = authenticationProvider;
        this.objectMapper = objectMapper;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/configuration/security",
                        "/swagger-ui.html/**",
                        "/webjars/**").permitAll()
                .antMatchers("/success").authenticated()
                .and()
                .addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                .formLogin()
                .successForwardUrl("/success")
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint);


    }


    @Bean
    public RestAuthenticationFilter authenticationFilter() throws Exception {
        RestAuthenticationFilter filter = new RestAuthenticationFilter(DEFAULT_FILTER_PROCESSES_URL,objectMapper);
        filter.setAuthenticationManager(this.authenticationManager());
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {

        auth.authenticationProvider(authenticationProvider);
    }




}
