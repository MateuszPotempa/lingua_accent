package com.linguaaccent.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinguaAccentApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinguaAccentApplication.class, args);
	}

}
