package com.linguaaccent.server.repository;


import com.linguaaccent.server.model.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class UserRepository {

    private static final String COLLECTION_NAME="users";

    private static final String EMAIL_FIELD="email";

    private final MongoOperations mongoOperations;

    @Autowired
    public UserRepository(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }


    public UserPrincipal save(UserPrincipal userPrincipal) {
        return mongoOperations.save(userPrincipal, COLLECTION_NAME);
    }

    public UserPrincipal findByEmail(String email){
        Query matchingQuery = query(where(EMAIL_FIELD).is(email));
        return mongoOperations.findOne(matchingQuery,UserPrincipal.class,COLLECTION_NAME);
    }

}
