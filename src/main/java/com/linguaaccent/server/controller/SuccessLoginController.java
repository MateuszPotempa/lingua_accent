package com.linguaaccent.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class SuccessLoginController {


    @GetMapping(path = "/success")
    public String hello() {
        return "Zalogowano";
    }
}
