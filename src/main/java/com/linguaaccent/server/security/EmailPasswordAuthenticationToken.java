package com.linguaaccent.server.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;

@Getter
@Setter
public class EmailPasswordAuthenticationToken extends AbstractAuthenticationToken {

    private  String email;
    private  String password;

    public EmailPasswordAuthenticationToken(String email, String password) {
        super(null);
        this.email = email;
        this.password = password;
        this.setAuthenticated(false);
    }

    public EmailPasswordAuthenticationToken(String email, String password, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.email = email;
        this.password = password;
        this.setAuthenticated(true);

    }

    @Override
    public Object getCredentials() {
        return this.email;
    }

    @Override
    public Object getPrincipal() {
        return this.password;
    }

}
