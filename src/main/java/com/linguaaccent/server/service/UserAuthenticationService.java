package com.linguaaccent.server.service;


import com.linguaaccent.server.dto.LoginRequest;
import com.linguaaccent.server.dto.SignUpRequest;
import com.linguaaccent.server.model.UserPrincipal;
import com.linguaaccent.server.repository.UserRepository;
import com.linguaaccent.server.security.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;

import static org.junit.platform.commons.util.StringUtils.isBlank;

@Service
@Slf4j
public class UserAuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsServiceImpl userDetailsService;

    @Autowired
    public UserAuthenticationService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserDetailsServiceImpl userDetailsService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }

    public void signUp(SignUpRequest signUpRequest) {
        log.debug("Sign-up request received: {}", signUpRequest);

        if (isBlank(signUpRequest.getEmail()) || isBlank(signUpRequest.getPassword()) || isBlank(signUpRequest.getConfirmPassword())) {
            log.error("Credentials not provided");
            //throw exception
        }

        log.debug("Checking password matching");

        if (!signUpRequest.getPassword().equals(signUpRequest.getConfirmPassword())) {
            log.error("Passwords do not match");
            //throw exception
        }

        UserPrincipal userPrincipal = userRepository.findByEmail(signUpRequest.getEmail());

        if (!Objects.isNull(userPrincipal)) {
            log.error("Sign-up failed! User with provided credentials already exists in database!");
            //throw error
        }
        userPrincipal = UserPrincipal.builder()
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .isActive(true)
                .authorities(Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")))
                .build();

        userRepository.save(userPrincipal);
    }

}
